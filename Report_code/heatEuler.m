function Un = heatEuler(a, b, J, tf, N, u0)
% HEAT EULER Solve the heat equation in 2D with Dirichlet boundary
% conditions with the explicit Euler scheme.
%   U = heatEuler(a, b, J, tf, N, u0) solves the heat equation
%   on mesh [a,b]x[a,b] in (x,y), at time tf. Use J mesh points in x and y and N timesteps. 
%   Begin condition is function u0, and velocity 0, boundary conditions are homogeneous Dirichlet;
    
    % Calculate mesh parameters
    dx = (b-a)/(J-1);
    dt = tf/N;
    mu = dt/dx^2;

    % Apply the initial conditions u0
    jx = 2:1:(J-1);
    [X, Y] = meshgrid((jx-1)*dx, (jx-1)*dx);
    Un = zeros(J, J);
    Un(jx, jx) = u0(X, Y);
    
    % Time stepping using the Explicit Euler method
    for n = 1:N
       Un(jx, jx) = mu*(Un(jx-1, jx)+Un(jx+1, jx)) + mu*(Un(jx, jx+1)+Un(jx, jx-1)) + (1 -4*mu)*Un(jx, jx);
    end
end