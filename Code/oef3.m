%% Define initial condition
u0 = @(x,y) sin( pi*x) .* sin(pi*y);
ustep = @(x, y) heaviside(0.5-x) .* heaviside(0.5-y);

%% Heat equation
uheatexact = @(x, y, t) sin(pi*x) .* sin(pi*y) .* exp(-2*t*pi*pi);
mu = 0.25;
tf = 0.1;
J = 11;
dx = 1/(J-1);
maxerror = [];
spacestep = [];

for i = 1:5
    i
    dt = mu*dx*dx;
    N  = tf/dt;

    U  = heatEuler(0, 1, 0, 1, J, J, tf, N, u0, false);
    [X, Y] = meshgrid(dx:dx:(1-dx), dx:dx:(1-dx));
    ueval = uheatexact(X, Y, tf);
    U = U(2:(end-1), 2:(end-1), end);

    maxerror = [maxerror, max(max(abs(ueval-U)))];
    spacestep = [spacestep, dx];
    
    dx = dx/2;
    J = 2*(J-1)+1;
end
second = [0.1, 0.01, 0.001];
loglog(spacestep, maxerror);
hold on;
loglog(second, second .^2);
xlabel('dx');
ylabel('Emax');
legend('Maximum error', 'Second order convergence');
title('Convergence order in space of the 2D heat equation');


%% Table for the heat equation
uheatexact = @(x, y, t) sin(pi*x) .* sin(pi*y) .* exp(-2*t*pi*pi);
tf = 0.1;
J = 6;
dx = 1/(J-1);
errors = zeros(4,1);

for j = 1:4
    dx
    [X, Y] = meshgrid(0:dx:1, 0:dx:1);
    dt = mu*dx*dx;
    N = ceil(tf/dt);
    
    U = heatEuler(0,1,0,1,J, J, tf, N, u0, false);
    ueval = uheatexact(X, Y, tf);
    err = max(max(abs(U-ueval)));
    errors(j) = err;
    
    dx = dx/2;
    J = 2*(J-1)+1;
end

%% Table for the wave equation
uwaveexact = @(x, y, t) sin(pi*x) .* sin(pi*y) .* cos(sqrt(2)*pi*t);
nu = 0.4;
tf = 0.1;
J = 6;
dx = 1/(J-1);
error = [];
spacestep = [];

for i = 1:4
    dt = nu*dx;
    N  = ceil(tf/dt);

    U  = waveEuler(0, 1, 0, 1, J, J, tf, N, u0, false);
    [X, Y] = meshgrid(dx:dx:(1-dx), dx:dx:(1-dx));
    ueval = uwaveexact(X, Y, tf);
    U = U(2:(end-1), 2:(end-1), end);

    error = [error, max(max(abs(ueval-U)))];
    spacestep = [spacestep, dx];
    
    dx = dx/2;
    J = 2*(J-1)+1;
end

%% Wave equation
uwaveexact = @(x, y, t) sin(pi*x) .* sin(pi*y) .* cos(sqrt(2)*pi*t);
nu = 1/sqrt(2);
tf = 0.1;
J = 11;
dx = 1/(J-1);
maxerror = [];
spacestep = [];

for i = 1:8
    dt = nu*dx;
    N  = ceil(tf/dt);

    U  = waveEuler(0, 1, 0, 1, J, J, tf, N, u0, false);
    [X, Y] = meshgrid(dx:dx:(1-dx), dx:dx:(1-dx));
    ueval = uwaveexact(X, Y, tf);
    U = U(2:(end-1), 2:(end-1), end);

    maxerror = [maxerror, max(max(abs(ueval-U)))];
    spacestep = [spacestep, dx];
    
    dx = dx/2;
    J = 2*(J-1)+1;
end
second = [0.1, 0.01, 0.001];
loglog(spacestep, maxerror);
hold on;
loglog(second, second.^2);
xlabel('dx');
ylabel('Emax');
legend('Maximum error', 'Second order convergence');
title('Convergence order in space of the 2D wave equation');


%% Transport equation
utransportexact = @(x, y, t) u0(x+t, y+t) .* heaviside(1-x-t) .* heaviside(1-y-t);
nu = 0.5;
tf = 0.1;
J = 11;
dx = 1/(J-1);
maxerror = [];
spacestep = [];

for i = 1:6
    dt = nu*dx;
    dt
    N  = ceil(tf/dt);
    
    U  = transportUpwind(0, 1, 0, 1, J, J, tf, N, u0, false);
    [X, Y] = meshgrid(0:dx:1, 0:dx:1);
    ueval = utransportexact(X, Y, tf);
    
    maxerror = [maxerror, max(max(abs(ueval-U)))];
    spacestep = [spacestep, dx];
    
    dx = dx/2;
    J = 2*(J-1)+1;
end

first = [0.1 0.01 0.001];
loglog(spacestep, maxerror);
hold on;
loglog(first, sqrt(first));
xlabel('dx');
ylabel('Emax');
legend('Maximum error', 'Order 0.5');

%% Table for the transport equation
utransportexact = @(x, y, t) u0(x+t, y+t) .* heaviside(1-x-t) .* heaviside(1-y-t);
tf = 0.1;
J = 6;
nu = 0.5;
dx = 1/(J-1);
errors = zeros(4,1);

for j = 1:4
    [X, Y] = meshgrid(0:dx:1, 0:dx:1);
    dt = nu*dx;
    N = ceil(tf/dt);
    U = transportUpwind(0,1,0,1,J, J, tf, N, u0, false);
    ueval = utransportexact(X, Y, tf);
    err = max(max(abs(U-ueval)));
    errors(j) = err;
     
    dx = dx/2;
    J = 2*(J-1)+1;
end
