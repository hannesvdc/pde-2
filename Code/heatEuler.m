function Un = heatEuler(a, b, c, d, Jx, Jy, tf, N, u0, movie)
% HEAT EULER Solve the heat equation in 2D with Dirichlet boundary
% conditions with the explicit Euler scheme.
%   U = heatEuler(a, b, c, d, Jx, Jy, tf, N,, u0) solves the heat equation
%   on mesh [a,b]x[c,d] in (x,y), at time tf. Use Jx mesh points in x, Jy in y and N timesteps. 
%   Begin condition is function u0, and velocity 0, boundary conditions are homogeneous Dirichlet;
    
    % Calculate mesh parameters
    dx = (b-a)/(Jx-1);
    dy = (d-c)/(Jy-1);
    dt = tf/N;
    mux = dt/dx^2;
    muy = dt/dy^2;  
    if mux + muy > 1/2
        disp(['Warning: Numerical solution is unstable, mux + muy = ', num2str(mux+muy)]);
    end
    
    % Apply the initial conditions u0, and ghost level at time -dt
    jx = 2:1:(Jx-1);
    jy = 2:1:(Jy-1);
    [X, Y] = meshgrid((jx-1)*dx, (jy-1)*dy);
    Un = zeros(Jx, Jy);
    Un(jx, jy) = u0(X, Y);
    
    % Make a movie of the solution
    if movie
        x = a:dx:b;
        y = c:dy:d;
        figure('Renderer','zbuffer');
        axis([0 1 0 1 -1 1 -1 1]);
        surf(x, y, Un);
        M(1) = getframe;
    end
    
    % Time stepping using the Explicit Euler method
    % No need to reapply boundary conditions.
    % We already have the solution at timestep 0 and 1, start with 2.
    for n = 1:N
       Un(jx, jy) = mux*(Un(jx-1, jy)+Un(jx+1, jy)) + muy*(Un(jx, jy+1)+Un(jx, jy-1)) + ...
                    (1 - 2*(mux + muy))*Un(jx, jy);

       % Update movie
       if movie
           surf(x, y, Un);
           axis([0 1 0 1 -1 1 -1 1]);
           xlabel('x');
           ylabel('y');
           zlabel('u(x,y,t)');
           title('Solution of the heat equation');
           M(n+1) = getframe;
       end

    end