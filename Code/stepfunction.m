%%
function r = stepfunction(x)
    if x == 0 
        r = 1;
    else 
        r = heaviside(x);
    end
end