%% Define initial condition
u0 = @(x,y) 15*(x-x.^2).*(y-y.^2).*exp(-50*((x-0.5).^2+(y-0.5).^2));

%% Heat equation
figure
mu = 0.25;
tf = 0.1;
J = 101;
dx = 1/(J-1);
dy = dx;
dt = mu*dx*dx;
N  = tf/dt;
U  = heatEuler(0, 1, 0, 1, J, J, tf, N, u0, false);
x = 0:dx:1;
y = 0:dy:1;
subplot(1,2,1);
surf(x, y, U);
caxis([0 0.02])
title('Solution heat equation at t_f = 0.1')
xlabel('x')
ylabel('y')
zlabel('u(x,y,0.1)')

tf = 0.2;
N  = tf/dt;
U  = heatEuler(0, 1, 0, 1, J, J, tf, N, u0, false);
subplot(1,2,2);
surf(x, y, U);
colormap('hot')
axis([0 1 0 1 0 0.03])
caxis([0 0.02])
title('Solution heat equation at t_f = 0.2')
xlabel('x')
ylabel('y')
zlabel('u(x, y, 0.2');

%% Wave equation 
figure
nu = 0.6;
tf = 0.1;
J = 101;
dx = 1/(J-1);
dy = dx;
dt = nu*dx;
N  = tf/dt;
U  = waveEuler(0, 1, 0, 1, J, J, tf, N, u0, false);
x = 0:dx:1;
y = 0:dy:1;
subplot(1,2,1);
surf(x, y, U);
caxis([0 0.3])
colormap('parula')
title('Solution wave equation at t_f = 0.1')
xlabel('x')
ylabel('y')
zlabel('u(x, y, 0.1)')
tf = 0.2;
N  = tf/dt;
U  = waveEuler(0, 1, 0, 1, J, J, tf, N, u0, false);
subplot(1,2,2);
surf(x, y, U);
axis([0 1 0 1 0 0.4])
caxis([0 0.3])
title('Solution wave equation at t_f = 0.2')
xlabel('x')
ylabel('y')
zlabel('u(x, y, 0.2)')

%% Transport equation 
figure
nu = 0.5;
tf = 0.1;
J = 101;
dx = 1/(J-1);
dy = dx;
dt = nu*dx;
N  = tf/dt;
U  = transportUpwind(0, 1, 0, 1, J, J, tf, N, u0, false);
x = 0:dx:1;
y = 0:dy:1;
subplot(1,2,1);
surf(x, y, U);
caxis([0 0.8])
colormap('parula')
title('Solution transport equation at t_f = 0.1')
xlabel('x')
ylabel('y')
zlabel('u(x, y, 0.1)')
tf = 0.2;
N  = tf/dt;
U  = transportUpwind(0, 1, 0, 1, J, J, tf, N, u0, false);
subplot(1,2,2);
surf(x, y, U);
axis([0 1 0 1 0 1.0])
caxis([0 0.8])
title('Solution transport equation at t_f = 0.2')
xlabel('x')
ylabel('y')
zlabel('u(x, y, 0.2)')