function Un = transportUpwind(a, b, c, d, Jx, Jy, tf, N, u0, movie)
    % Calculate mesh parameters
    dx = (b-a)/(Jx-1);
    dy = (d-c)/(Jy-1);
    dt = tf/N;
    nux = dt/dx;
    nuy = dt/dy;

    % Apply the initial conditions u0, and ghost level at time -dt
    jx = 1:1:(Jx-1);
    jy = 1:1:(Jy-1);
    [X, Y] = meshgrid((jx-1)*dx, (jy-1)*dy);
    Un = zeros(Jx, Jy);
    Un(jx, jy) = u0(X, Y);
    
    % Make a movie of the 
    if movie
        x = a:dx:b;
        y = c:dy:d;
        figure('Renderer','zbuffer');
        axis([0 1 0 1 -1 1 -1 1]);
        surf(x, y, Un);
        M(1) = getframe;
    end
   
    for n = 1:N
       Un(jx, jy) = (1-nux-nuy)*Un(jx, jy) + nux*Un(jx+1, jy) + nuy*Un(jx, jy+1);
      
       if movie
           surf(x, y, Un);
           axis([0 1 0 1 -1 1 -1 1]);
           M(n+1) = getframe;
           xlabel('x');
           ylabel('y');
           zlabel('u(x,y, t)');
           title('Transport equation');
       end
    end

end