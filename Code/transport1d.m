function U = transport1d(a, b, J, tf, N, u0)
    dx = (b-a)/(J-1);
    dt = tf/N;
    nu = dt/dx;
    
    jx = 2:1:J;
    U(jx) = u0((jx-1)*dx);
    
    for i = 1:N
        U(jx) = (1-nu)*U(jx) + nu*U(jx-1);
    end
end