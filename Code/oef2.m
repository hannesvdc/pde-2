%% Define the initial condition for all problems.
u0 = @(x, y) 15 .* (x - x.^2)*(y - y.^2)*exp(-50 * ((x - 0.5).^2 + (y - 0.5).^2));

%% Stability analysis of the heat equation
disp('Heat Equation Stable');
tf = 1;
J = 11;
dx = 1/(J-1);
dt = dx^2 / 4;
N = ceil(tf/dt);
dt = tf/N;
disp(['Stable algorithm: mu = ', num2str(dt/dx^2)]);
Uhs = heatEuler(0,1,0,1, J, J, tf, N, u0, true);

%% Heat equation unstable
disp('Heat equation unstable');
tf = 2.0;
J = 11;
dx = 1/(J-1);
mu = 0.26;
dt = mu*dx*dx;
N = ceil(tf/dt) - 10;
dt = tf/N;
disp(['Unstable algorithm: mu = ', num2str(dt/dx^2)]);
Uhuns = heatEuler(0,1,0,1, J, J, tf, N, u0, true);

%% Stability analysis of the wave equation: stable;
disp('Wave Equation');
tf = 1.5;
J = 11;
nu = 1/sqrt(2);
dx = 1/(J-1);
dt = dx*nu;
N = ceil(tf/dt);
Uws = waveEuler(0,1,0,1,J, J, tf, N, u0, true);

%% Stability analysis of the wave equation: unstable;
disp('Wave Equation');
tf = 1.5;
J = 11;
nu = 1/sqrt(2)+0.05;
dx = 1/(J-1);
dt = dx*nu;
N = ceil(tf/dt);
Uws = waveEuler(0,1,0,1,J, J, tf, N, u0, true);



%% Stability analysis of the transport equation: nu <= 0.5;
disp('Transport Equation');
tf = 1;
J = 11;
dx = 1/(J-1);
nu = 0.5;
dt = dx*nu;
N = ceil(tf/dt);
dt/dx
N
Uts = transportUpwind(0,1,0,1,J, J, tf, N, u0, true);

% N = ceil(tf/dt) - 10;
% Uwuns = transportUpwind(0,1,0,1,J, J, tf, N, u0);