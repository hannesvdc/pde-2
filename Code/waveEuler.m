function Un = waveEuler(a, b, c, d, Jx, Jy, tf, N, u0, movie)
% WAVEEULER Solve the wave equation in 2D with Dirichlet boundary
% conditions with the upwind scheme.
%   U = waveUpwind(a, b, c, d, Jx, Jy, tf, N,, u0) solves the wave equation
%   on mesh [a,b]x[c,d] in (x,y), at time tf. Use Jx+1 mesh points in x, Jy+1 in y and N timesteps. 
%   Begin condition is function u0, and velocity 0, boundary conditions are homogeneous Dirichlet;

    % Calculate mesh parameters
    dx = (b-a)/(Jx-1);
    dy = (d-c)/(Jy-1);
    dt = tf/N;
    nux = (dt/dx)^2;
    nuy = (dt/dy)^2;
    if dt/dx > 1/sqrt(2)
        disp(['Solution might be unstable, nu = ', num2str(dt/dx)]);
    end
    
    % Apply the initial conditions u0, and ghost level at time -dt
    jx = 2:1:(Jx-1);
    jy = 2:1:(Jy-1);
    [X, Y] = meshgrid((jx-1)*dx, (jy-1)*dy);
    Up = zeros(Jx, Jy);
    Un = zeros(Jx, Jy);

    Up(jx, jy) = u0(X, Y);
    Un(jx, jy) = 0.5*nux*( Up(jx-1, jy) + Up(jx+1, jy)) + ...
                 0.5*nuy*( Up(jx, jy-1) + Up(jx, jy+1)) + ...
                 (1 - nux - nuy )*Up(jx, jy);
    
    % Make a movie of the solution
    if movie
        x = a:dx:b;
        y = c:dy:d;
        figure('Renderer','zbuffer');
        axis([0 1 0 1 -15 15 ]);
        surf(x, y, Up);
        M(1) = getframe;
        surf(x, y, Un);
        M(2) = getframe;
    end

    % Time stepping using the Explicit Euler method
    % No need to reapply boundary conditions.
    % We already have the solution at timestep 0 and 1, start with 2.
    for n = 2:N
       Utemp = Un;
       Un(jx, jy) = nux*(Un(jx-1, jy)+Un(jx+1, jy)) + nuy*(Un(jx, jy+1)+Un(jx, jy-1)) + ...
                    2*(1-nux-nuy)*Un(jx, jy) - Up(jx, jy);
       Up = Utemp;
       
       % Update movie
       if movie
           surf(x, y, Un);
           axis([0 1 0 1 -15 15 -15 15]);
           M(n+1) = getframe;
       end
    end       
    
end