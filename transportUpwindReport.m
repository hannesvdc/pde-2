function Un = transportUpwind(a, b, J, tf, N, u0)
    % Calculate mesh parameters
    dx = (b-a)/(J-1);
    dt = tf/N;
    nu = dt/dy;

    % Apply the initial conditions u0
    jx = 1:1:(J-1);
    [X, Y] = meshgrid((jx-1)*dx, (jx-1)*dx);
    Un = zeros(J, J);
    Un(jx, jx) = u0(X, Y);
   
    % Time stepping
    for n = 1:N
       Un(jx, jx) = (1 - 2*nu)*Un(jx, jx) + nu*Un(jx+1, jx) + nu*Un(jx, jx+1);   
    end
end