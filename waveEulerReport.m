function Un = waveEuler(a, b, J, tf, N, u0)
% WAVEEULER Solve the wave equation in 2D with Dirichlet boundary
% conditions with an explicit scheme.
%   U = waveEuler(a, b, J tf, N, u0) solves the wave equation
%   on mesh [a,b]x[a,b] in (x,y), at time tf. Use J mesh points in x and y and N timesteps. 
%   Begin condition is function u0,, velocity 0 and the boundary conditions
%   are homogeneous Dirichlet.

    % Calculate mesh parameters
    dx = (b-a)/(J-1); dt = tf/N; nu = dt/dx;
    
    % Apply the initial conditions u0, and compute ghost level at time -dt
    j = 2:1:(J-1);
    [X, Y] = meshgrid((j-1)*dx, (j-1)*dx);
    Up = zeros(J, J); Un = zeros(J, J);

    Up(j, j) = u0(X, Y);
    Un(j, j) = 0.5*nu^2*( Up(j-1, j) + Up(j+1, j) + Up(j, j-1) + Up(j, j+1)) + (1 - 2*nu^2)*Up(j, j);
             
    % Time stepping using the Explicit Euler method
    for n = 2:N
       Utemp = Un;
       Un(j, jy) = nu^2*(Un(j-1, j)+ Un(j+1, j) + Un(j, j+1)+Un(j, j-1)) + 2*(1-2*nu^2)*Un(j, j) - Up(j, j);
       Up = Utemp;
    end
end