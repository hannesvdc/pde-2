\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}2D Heat equation}{1}{section.2}
\contentsline {subsection}{\numberline {2.1}MATLAB Implementation}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Correctness of the implementation}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Stability limitation}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Accuracy of explicit Euler}{3}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Application to another initial condition}{3}{subsection.2.5}
\contentsline {section}{\numberline {3}2D Wave equation}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}MATLAB Implementation}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Correctness of the implementation}{5}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Stability limitation}{5}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Accuracy of explicit scheme}{6}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Application to another initial condition}{6}{subsection.3.5}
\contentsline {section}{\numberline {4}2D Transport equation}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}MATLAB Implementation}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Correctness of the implementation}{7}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Stability limitation}{8}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Accuracy of the upwind scheme}{9}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Application to another initial condition}{9}{subsection.4.5}
