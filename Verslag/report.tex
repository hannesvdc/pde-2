\documentclass[a4paper,kul]{kulakarticle}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[binary-units=true]{siunitx}
\usepackage{listings}
\usepackage{color} %red, green, blue, yellow, cyan, magenta, black, white
\definecolor{mygreen}{RGB}{28,172,0} % color values Red, Green, Blue
\definecolor{mylilas}{RGB}{170,55,241}
\usepackage{changepage}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage[margin=8pt]{subfig}
\usepackage{listings}
\usepackage{amsthm}
\usepackage{amsmath}
\usepackage{float}
\usepackage{listings}
\usepackage{epstopdf}

\renewcommand\floatpagefraction{.9}
\renewcommand\dblfloatpagefraction{.9} % for two column documents
\renewcommand\topfraction{.9}
\renewcommand\dbltopfraction{.9} % for two column documents
\renewcommand\bottomfraction{.9}
\renewcommand\textfraction{.1} 

\date{Academic year 2016-2017}
\address{
	Master Mathematical Engineering \\
	Numerical methods for solving PDEs \\
	Prof. Dr. Ir. Stefan Vandewalle, Prof. Dr. Stefaan Poedts}
\title{Practicum 2: Numerical solutions for 2D parabolic and hyperbolic problems}
\author{Simon Geirnaert, Hannes Vandecasteele}

\begin{document}
	
	\lstset{language=Matlab,%
		%basicstyle=\color{red},
		breaklines=true,%
		linewidth=14cm,
		basicstyle=\scriptsize,
		morekeywords={matlab2tikz},
		keywordstyle=\color{blue},%
		morekeywords=[2]{1}, keywordstyle=[2]{\color{black}},
		identifierstyle=\color{black},%
		stringstyle=\color{mylilas},
		commentstyle=\color{mygreen},%
		showstringspaces=false,%without this there will be a symbol in the places where there is a space
		numbers=left,%
		numberstyle={\tiny \color{black}},% size of the numbers
		numbersep=9pt, % this defines how far the numbers are from the text
		emph=[1]{for,end,break},emphstyle=[1]\color{red}, %some words to emphasise
		%emph=[2]{word1,word2}, emphstyle=[2]{style},    
	}

	\maketitle
	
%	\tableofcontents
	
\section{Introduction}
In this report we present and implement three explicit numerical schemes for 2D parabolic and hyperbolic equations. For the 2D heat equation and wave equation we consider the explicit Euler scheme, while for the 2D transport equation the well-known upwind scheme is used. The report is divided in three sections, one for each equation. In each section, we show a listing of the code and analyze the correctness via order tests. Next we illustrate the stability of our implementation by veryfing the theoretical stability limits. Afterwards, the order of the methods is discussed in space and time and we finally calculate the numerical solution to an initial condition that has no analytical solution.

\section{2D Heat equation}
A well known equation in two dimensions is the so-called heat equation. It models the transfer of heat over a two dimensional region $\Omega = [0,1] \times [0,1]$
\[
\frac{\partial u}{\partial t} = \frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2}.
\]
We will assume homogeneous Dirichlet boundary conditions and a given initial condition $u_0(x,y)$. The explicit Euler scheme is one of the easiest ways of discretizing this equation. It is first order in time and second order in each space dimension, and is given by
\[
U^{n+1}_{r, s} = (1-2(\mu_x+\mu_y))U^{n}_{r,s}+\mu_x(U_{r-1,s}^n+U_{r+1,s}^n)+\mu_y(U_{r ,s+1}^n+U_{r, s-1}^n)
\]
where $\mu_x = \frac{\Delta t}{\Delta x^2}$ and $\mu_y = \frac{\Delta t}{\Delta y^2}$. In this report, $\mu_x = \mu_y$ because we will always assume $\Delta x = \Delta y$.

\subsection{MATLAB Implementation}
The condensed MATLAB code for this problem is given below.
\lstinputlisting{../heatEulerReport.m}

\subsection{Correctness of the implementation}
For the correctness, we propose the same test as in the previous report. We perform an order test in which we keep $\mu$ constant on a time-dependent problem where we know the analytical solution. Then, if we decrease the spatial discretization $\Delta x$, the maximal error should decrease as $\mathcal{O}(\Delta x^2, \Delta t) = \mathcal{O}(\Delta x^2) = \mathcal{O}(\Delta t)$. For this test, we solved the equation on $\Omega = [0,1] \times [0,1]$ with initial condition $u_0(x, y) = \sin(\pi x) \sin(\pi y)$. The exact solution is then given by $u(x, y, t) =  \sin(\pi x) \sin(\pi y) e^{-2\pi^2t}$. The convergence plot is shown in Figure \ref{fig:heatconv}.
\begin{figure}
\centering
\includegraphics[width=0.5\linewidth]{heatconv}
\caption{Second order convergence in space for the 2D heat equation.}
\label{fig:heatconv}
\end{figure}
On the figure we clearly see second order convergence in space, and hence first in time.

\subsection{Stability limitation}
An important theoretical result for the explicit Euler scheme for the heat equation is that the solution is only numerically stable iff $\mu_x+\mu_y \leq \frac{1}{2}$. As here $\mu_x = \mu_y$ this boils down to $\mu=0.25$. To test this boundary, we solved the heat equation with the same initial condition as in the previous section until time $t_f = 1$, first for $\mu \leq 0.25$. The solution is plotted in Figure \ref{fig:heatmu0.25}. We see that the solution behaves nicely as expected.
If however we take $\mu = 0.26$ and let the solution run until time $t_f=2$, then the solution numerically explodes and is shown in Figure \ref{fig:heatmu0.26}. This does not happen when we take $t_f=2$ for $\mu=0.25$. This clearly indicates that $\mu=0.25$ is the stability limit.

\begin{figure}
	\centering
	\subfloat[The numerical solution of the heat equation at time $t_f=1$. We see that the solution converges nicely to 0.]{\includegraphics[width=0.5\linewidth]{heatmu025}\label{fig:heatmu0.25}}
	\subfloat[The numerical solution of the heat equation at time $t_f=2$. We see that the solution becomes unstable and starts to explode.]{\includegraphics[width=0.5\linewidth]{heatmu026}\label{fig:heatmu0.26}}
\end{figure}

\subsection{Accuracy of explicit Euler}
As another confirmation of the accuracy of the explicit Euler scheme, we compute the error of this scheme at time $t_f=0.1$ on the model problem with initial condition $u_0(x, y) = \sin(\pi x) \sin(\pi y)$. We let $\Delta x$ decrease from $0.2$ to $0.025$ by a factor of 2, and choose $\Delta t$ such that $\mu=0.25$, on the stability boundary. The results are stated in Table \ref{tab:heat}.
\begin{table}
	\centering
	\begin{tabular}{c| c | c | c | c }
		$\Delta x$ & 0.2 & 0.1 & 0.05 & 0.025 \\
		\hline
		Error & \num{0.0170} & \num{0.0046} &  \num{0.0011}  & \num{2.8208e-4}  \\
	\end{tabular}
	\caption{Numerical errors for the heat equation of the example problem, for $\mu = 0.25$. From the table we read second order in space and first order in time.}
	\label{tab:heat}
\end{table}
The numerical results are quite clear. We see that for $\mu=0.25$ the convergence is of order 2 in space because the error is divided by four in each step. This also indicates first order in time.

\subsection{Application to another initial condition}
Finally for the explicit Euler scheme, we apply the method to the initial condition $u_0(x,y)=15(x - x^2)(y - y^2)e^{(-50((x - 0.5)^2 + (y - 0.5)^2))}$. The analytical solution is hard to calculate for this initial condition. The numerical solution at time $t_f=0.1$ and $t_f=0.2$ is plotted in Figure \ref{fig:heatexp}. Here again we see that initial condition is damped and spreads out over the area $\Omega$.
\begin{figure}
\centering
\includegraphics[width=0.7\linewidth]{heatexp}
\caption{The numerical solution of the heat equation at times $t_f=0.1$ and $t_f=0.2$.}
\label{fig:heatexp}
\end{figure}

\section{2D Wave equation}
The 2D wave equation gives a description of a wave in two dimensions, over time:
\[
\frac{\partial^2 u}{\partial t^2} = \frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^2}
\]
over a unit square $\Omega = [0,1] \times [0,1]$, with homogeneous Dirichlet boundary conditions and given initial condition $u_0(x,y)$. We also assume that the initial velocity $\frac{\partial u}{\partial t}(x, y, 0) = u_1(x,y) = 0$. To discretize this scheme, we use an explicit method with central differences:
\[
U^{n+1}_{r,s} = \nu_x^2(U^n_{r+1,s}+U^n_{r-1,s})+\nu_y^2(U^n_{r,s+1}+U^n_{r,s-1})+2(1-\nu_x^2-\nu_y^2)U^n_{r,s}-U^{n-1}_{r,s},
\]
where $\nu_x = \frac{\Delta t}{\Delta x}$ and $\nu_y = \frac{\Delta t}{\Delta y}$. We again assume that $\nu_x = \nu_y$. This scheme is only useful when the solution on two timesteps is known. To obtain the solution on the first timestep $U^1_{r,s}$, we could use the second initial condition and discretize this with forward differences. This gives us only first order, while the scheme implements a second order scheme. To preserve the second order, we introduce a \emph{ghost} level at time $-\Delta t$. We obtain this ghost level by using a double distance difference on the second initial condition. If we then plug in the solution for this ghost level in the discretized equation for the level at time $\Delta t$, we obtain:
\[
U^{1}_{r,s} = \frac{\nu_x^2}{2}(U^0_{r+1,s}+U^0_{r-1,s})+\frac{\nu_y^2}{2}(U^0_{r,s+1}+U^0_{r,s-1})+(1-\nu_x^2-\nu_y^2)U^0_{r,s}+\Delta t u_1(x_r,y_s).
\]
Note that, in the implementation, we plugged in $u_1(x_r,y_s) = 0$. This second level is obtained by using second order schemes, so we preserve the second order of our solution.

\subsection{MATLAB Implementation}
The condensed MATLAB code for this problem is given below. Note that we didn't include the code to make the movie, to not overload the code.
\lstinputlisting{../waveEulerReport.m}

\subsection{Correctness of the implementation}
\label{subsec:correctnessWave}
To verify the correctness of the code, we perform an order test in which we keep $\nu$ constant. If we decrease the spatial discretization $\Delta x$, the maximal error should decrease as $\mathcal{O}(\Delta x^2, \Delta t^2) = \mathcal{O}(\Delta x^2) = \mathcal{O}(\Delta t^2)$. To test this, we solved the equation on a time-dependent problem with initial condition $u_0(x, y) = \sin(\pi x) \sin(\pi y)$. The exact solution is then given by $u(x, y, t) =  \sin(\pi x) \sin(\pi y) \cos(\sqrt{2}\pi t)$. Figure \ref{fig:waveconv} shows the convergence plot.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{waveconv}
	\caption{Second order convergence in space for the 2D wave equation.}
	\label{fig:waveconv}
\end{figure}
On the figure we clearly see second order convergence in space, and hence second in time.

\subsection{Stability limitation}
First we prove that for the 2D wave equation, for our scheme, the solution is numerically stable if and only if $\nu \leq \frac{\sqrt{2}}{2}$:
\begin{proof}
	Say $U^n_{r,s} = \lambda^n e^{i(k_xr\Delta x+k_ys\Delta y)}$. We consider our discretized equation and plug in this expression:
	\begin{equation*}
	\begin{split}
		U^{n+1}_{r,s} &  =  (2+\nu_x^2\delta_x^2+\nu_y^2\delta_y^2)U^n_{r,s} - U^{n-1}_{r,s} \\
		\Rightarrow & \lambda^2 - 2\underbrace{(1-2\nu_x^2\sin(k_x \Delta x/2)-2\nu_y^2\sin(k_y \Delta y/2))}_{b}\lambda + 1 = 0 \\
		\Rightarrow & \lambda = b \pm \sqrt{b^2-1}
	\end{split}
	\end{equation*}
	In order to make $|\lambda| \leq 1$, take $-1 \leq b \leq 1$. This implies that:
	\begin{equation*}
		\begin{split}
		-1 \leq & 1-2\nu_x^2-2\nu_y^2 \leq 1 \\
		\Rightarrow & \nu_x^2 + \nu_y^2 \leq 1,
		\end{split}
	\end{equation*}
	where we put both $\sin(k_x \Delta x/2)$ and $\sin(k_y \Delta y/2)$ to one. If $\nu_x = \nu_y$, we get $\nu \leq \frac{\sqrt{2}}{2}$.
\end{proof}
Note that this is equivalent to the CFL-condition, what is not generally the case.

To test this boundary numerically, we solved the wave equation with initial condition $u_0(x,y) = 15(x - x^2)(y - y^2)e^{-50((x - 0.5)^2 + (y - 0.5)^2)}$, until time $t_f = 1.5$, first for $\nu = 1/\sqrt{2}$. Figure \ref{fig:wavenustable} shows the solution. It behaves as expected.
If however we take $\nu = 0.75$ and let the solution run until time $t_f=1.5$, then the solution numerically explodes. This is shown in Figure \ref{fig:wavenuunstable}.

\begin{figure}
	\centering
	\subfloat[The numerical solution of the wave equation at time $t_f=1.5$. We see that the solution nicely oscillates bounded.]{\includegraphics[width=0.5\linewidth]{wavenustable}\label{fig:wavenustable}}
	\subfloat[The numerical solution of the wave equation at time $t_f=1.5$. We see that the solution becomes unstable and starts to explode.]{\includegraphics[width=0.5\linewidth]{wavenuunstable}\label{fig:wavenuunstable}}
\end{figure}

\subsection{Accuracy of explicit scheme}
Besides the order tests with initial solution $u_0(x,y,t) = \sin(\pi x)\sin(\pi y)$ we already gave in Section \ref{subsec:correctnessWave}, we can also confirm the accuracy of the scheme by computing the error of this scheme at time $t_f=0.1$. We let $\Delta x$ decrease from $0.2$ to $\num{0.025}$ by a factor of 2, and consequently $\Delta t$ from  $0.1414$ to $\num{0.017678}$, such that $\nu = 1/\sqrt{2}$ is constant. The results are shown in Table \ref{tab:wave}. We also show the results for $\nu = 0.4$.

\begin{table}
	\centering
\begin{tabular}{c| c | c | c | c }
		$\nu$ / $\Delta x$ & 0.2 & 0.1 & 0.05 & 0.025 \\
		\hline
	$1/\sqrt{2}$ & \num{1.4397e-03} & \num{3.9397e-04} &  \num{2.1860e-05}  & \num{5.4563e-06}  \\
	0.4 & \num{2.4707e-03} & \num{6.1062e-04} &  \num{1.3353e-04}  & \num{3.3377e-05}  \\
	\end{tabular}
	\caption{Numerical errors for the wave equation of the example problem, for $\nu = 1/\sqrt{2}$ and $\nu = 0.4$. From the table we read second order in space and time.}
	\label{tab:wave}
\end{table}

The numerical results are quite clear. The table shows second order convergence in space and time, because the error is divided by 4 if $\Delta x$ halves. As we know that $\nu = \frac{\Delta t}{\Delta x}$, second order convergence also appears in time.

\subsection{Application to another initial condition}
Finally for the explicit scheme, we apply the method to the initial condition $u_0(x,y)=15(x - x^2)(y - y^2)e^{-50((x - 0.5)^2 + (y - 0.5)^2)}$. The numerical solution at time $t_f=0.1$ and $t_f=0.2$ is plotted in Figure \ref{fig:waveexp}. We choose $\nu = 0.6$, under the stability limit. You see indeed the wave movement.
\begin{figure}
	\centering
	\includegraphics[width=\linewidth]{waveexp}
	\caption{The numerical solution of the wave equation at times $t_f=0.1$ and $t_f=0.2$.}
	\label{fig:waveexp}
\end{figure}


\section{2D Transport equation}
Finally we turn to the two dimensional transport equation, given by
\[
\frac{\partial u}{\partial t} = \frac{\partial u}{\partial x} + \frac{\partial u}{\partial y}
\]
where the velocities in the $x$ and $y$ directions are 1. The initial condition will propagate to the lower left, hence the analytical solution is $u(x, y, t) = u_0(x+t, y+t)$. The boundary conditions are again homogeneous Dirichlet conditions and these will also propagate to the lower left corner of the domain as the characteristics are parallel to the first bissectrice of the plane.
To discretize the equation, we used the well-known upwind scheme. The intial condition is propagated to the left, therefore we should use forward differences to approximate the equation. Then we get the following
\[
U_{r,s}^{n+1} = (1 - \nu_x - \nu_y) U_{r,s}^{n} + \nu_x U_{r+1,s}^n + \nu_y U_{r, s+1}^n,
\]
with $\nu_x = \frac{\Delta t}{\Delta x}$ and  $\nu_y = \frac{\Delta t}{\Delta y}$. Here again $\nu_x=\nu_y$ because $\Delta x = \Delta y$.

\subsection{MATLAB Implementation}
The condensed MATLAB code for this equation is given here.
\lstinputlisting{../transportUpwindReport.m}

\subsection{Correctness of the implementation}
To check the correctness of the implementation, we will again carry out a convergence test. Caution is needed here. Because the solution $u_0(x+t, y+t)$ has a discontinuous first derivative due to the propagation of the boundary conditions, the trunctation error is not $\mathcal{O}(\Delta x)$ but only $\mathcal{O}(\sqrt{\Delta x})$. This will also be the order of convergence if we keep $\nu$ constant. More specifically, the error will decrease by a factor $\sqrt{2}$ if we halve $\Delta x$ and $\Delta t$. 
For the actual test, we again consider a time-dependent problem with initial condition $u_0(x,y) = \sin(\pi x) \sin(\pi y)$. The convergence plot is shown in Figure \ref{fig:transportconv}.
\begin{figure}
	\centering
	\includegraphics[width=0.4\linewidth]{transportconv}
	\caption{Convergence order of the 2D transport equation if we keep $\nu$ constant. We clearly see that the maximal error decreases as $\mathcal{O}(\sqrt{\Delta x})$.}
	\label{fig:transportconv}
\end{figure}
We clearly see that the order of convergence is 0.5, a strong indication that the implementation is correct.

\subsection{Stability limitation}
The stability criterion for the 1D transport equation is $\nu \leq 1$ and it can be proven that the criterion for the 2D problem is $\nu \leq 0.5$ when $\Delta x= \Delta y$. We will now confirm this numerically. Figure \ref{fig:transport05} shows the numerical result when we apply the transport equation with $\nu = 0.5$ at time $t_f=1$. We can see that the initial condition has completely left the region and that there are no ripples. If however we choose $\nu=0.6$ in Figure \ref{fig:transport06} then there are ripples left after the initial pattern left the region. This indicates instability. If however we had chosen $\nu=0.52$ there would still have been ripples, but they wouldn't have been very visible because they are quite small.
\begin{figure}
	\subfloat[Stable numerical solution of the transport equation with $\nu= 0.5$. We observe no ripples.]{\includegraphics[width=0.5\linewidth]{transport05}\label{fig:transport05}}
	\subfloat[Unstable numerical solution of the transport equation with $\nu=0.6$. Now we do see ripples that indicate instability.]{\includegraphics[width=0.5\linewidth]{transport06}\label{fig:transport06}}
\end{figure}
\subsection{Accuracy of the upwind scheme}
To check the accuracy of our implementation, we calculate the maximum error at time $t_f = 0.1$ of our approximation to the model problem with initial condition $u_0(x, y) = \sin(\pi x) \sin(\pi y)$. The analytical solution in this case is $u_0(x+t, y+t)$. The maximum error should scale as $\mathcal{O}(\sqrt{\Delta x})$. Table \ref{tab:transport} contains the results for a constant value of $\nu=0.5$ where $\Delta x$ decreases from 0.2 to 0.025.
\begin{table}
	\centering
	\begin{tabular}{c| c | c | c | c }
		$\Delta x$ & 0.2 & 0.1 & 0.05 & 0.025 \\
		\hline
		Error & \num{0.0955} & \num{0.0773} &  \num{0.0582}  & \num{0.0427}  \\
	\end{tabular}
	\caption{Numerical errors for the transport equation of the example problem, for $\nu = 0.5$. From the table we half an order convergence in space and time.}
	\label{tab:transport}
\end{table}
In the table we clearly see that the error is divided by $\sqrt{2}$ when $\Delta x$ is halved, indicating that the order of convergence is 0.5 in space and time.

\subsection{Application to another initial condition}
Finally we turn again to applying the upwind scheme to another initial condition, the same as for the previous equations. Figure \ref{fig:transportexp} shows the numerical results for $t_f = 0.1$ and $t_f = 0.2$.
On the figures we see that the initial condition propagates to the lower left corner, while there is a slight damping in amplitude of the shape. This is also a well-known property of the upwind scheme.
\begin{figure}
	\centering
	\includegraphics[width=0.7\linewidth]{transportexp}
	\caption{The numerical solution of the transport equation using the upwind scheme to an exponential initial condition.}
	\label{fig:transportexp}
\end{figure}


\end{document}